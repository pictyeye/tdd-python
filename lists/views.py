from django.shortcuts import render, redirect
from django.core.exceptions import ValidationError
from lists.models import Item, List

def home_page(request):
    return render(request, 'home.html')

def view_list(request, list_id):
    l = List.objects.get(id=list_id)
    return render(request, 'list.html', {'list' : l})

def new_list(request):
    l = List.objects.create()
    item = Item.objects.create(text=request.POST['item_text'], list=l)
    try:
        item.full_clean()
        item.save()
    except ValidationError:
        l.delete()
        error = "You can't have an empty list item"
        return render(request, 'home.html', {'error': error})
    return redirect(f'/lists/{l.id}/')

def add_item(request, list_id):
    l = List.objects.get(id=list_id)
    Item.objects.create(text=request.POST['item_text'], list=l)
    return redirect(f'/lists/{l.id}/')
