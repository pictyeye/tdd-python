#!/bin/bash


/etc/init.d/nginx restart


cd /root/SITENAME

export DJANGO_DEBUG_FALSE=y
export DJANGO_SECRET_KEY="$(tr -cd 'a-zA-Z-0-9' < /dev/urandom | head -c 64)"

if [ $# -eq 0 ]; then
	virtualenv/bin/gunicorn --bind \
		unix:/tmp/SITENAME.socket \
		superlists.wsgi:application
else
	virtualenv/bin/python manage.py "$@"
fi
